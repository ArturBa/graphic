/*
 * main.c
 *
 * Copyright 2017 Artur Bauer <artur@e7270>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#pragma pack(1)

#ifndef BOOL_H
#define BOOL_H
    typedef enum {false=0, true=1} bool;
#endif  // BOOL_H



typedef struct{
	char name[100];
	int width, height;
	unsigned short int grey_value;
	unsigned short int **tab;
	unsigned short int standard; //acceptatble only p2, p5 for pgm
}picture;

const picture* picture_add(void);
void picture_save(picture* const);
void picture_show_base(short int, picture**);
void picture_select(short int*, short int, picture**);
void picture_mirror_0Y_axis(picture* const);
void picture_mirror_0X_axis(picture* const);
void picture_negative(picture* const);
void picture_histogram(picture* const);
void picture_rotate_clockwise(picture* const);
void picture_delete(picture**, short int);
void picture_mediana_filter(picture* const);
void picture_salt_pepper_filter(picture* const);
void picture_proging(picture* const);
void picture_histogram_append(picture* const);
void picture_resize(picture* const);
void picture_histogram_equalization(picture* const);
void picture_generic_filter(picture* const, bool); //if bool==true is gausian
double* picture_histogram_array(picture* const);
double array_biggest_double(double*, int);

short int menu_t(short int);
void menu_rotate(picture* const);
short int menu_rotate_t(void);
void menu_picture(short int*, short int*, picture***);
short int menu_picture_t(void);
void menu_filter(picture* const);
short int menu_filter_t(void);
short int menu_histogram_t(void);
void menu_histogram(picture* const);

int main(int argc, char **argv){
	short int picture_counter=0;
	short int picture_selected=0;
	picture** picture_array;

	while(1){
	switch(menu_t(picture_selected)){
        case 1:
            menu_picture(&picture_selected,&picture_counter, &picture_array);
            break;
        case 2:
            if(picture_selected==0){
                fprintf(stderr ,"First you need to select picture.\n");
                if(picture_counter==0){
                    fprintf(stderr,"ERROR. Cannot delete picture, because picture base is void\n");
					getchar();
					break;
                }
                getchar();
                picture_select(&picture_selected, picture_counter, picture_array);
            }
            menu_rotate(*(picture_array+picture_selected-1));
            break;
        case 3:
            if(picture_selected==0){
                fprintf(stderr ,"First you need to select picture.\n");
                if(picture_counter==0){
                    fprintf(stderr,"ERROR. Cannot delete picture, because picture base is void\n");
					getchar();
					break;
                }
                getchar();
                picture_select(&picture_selected, picture_counter, picture_array);
            }
            menu_filter(*(picture_array+picture_selected-1));
            break;
        case 4:
            if(picture_selected==0){
                fprintf(stderr ,"First you need to select picture.\n");
                if(picture_counter==0){
                    fprintf(stderr,"ERROR. Cannot delete picture, because picture base is void\n");
					getchar();
					break;
                }
                getchar();
                picture_select(&picture_selected, picture_counter, picture_array);
            }
            menu_histogram(*(picture_array+picture_selected-1));
            break;
        case 5:
            picture_resize(*(picture_array+picture_selected-1));
            break;
        case 0:
            for(picture_selected=0;picture_selected<picture_counter;picture_selected++){
                int j=0;
                picture* picture_ptr=*(picture_array+picture_selected);
                for(j=0;j<(picture_ptr->height);j++){
                    free(*(picture_ptr->tab+j));
                }
                free(picture_ptr->tab);
                free(picture_ptr);
                free(*(picture_array+picture_selected));
            }
            free(picture_array);
            return 0;
        default:
            break;
	}
	}
}

short int menu_t(short int picture_selected){
    short int buffor;
    while(1){
    system("clear");
    printf("\
Selected picture: %hd\n\
MENU:\n\
1. MENU picture.\n\
2. MENU rotate.\n\
3. MENU filter.\n\
4. MENU histogram.\n\
5. Resize.\n\
0. Exit.\n\
\
Type here: ",picture_selected);
    if(scanf("%hd",&buffor)==1)
        return buffor;
    else
        while(getchar()!='\n');
    }
}

short int menu_rotate_t(void){
    short int buffor;
    while(1){
    system("clear");
    printf("\
ROTATE MENU:\n\
1. Rotate clockwise.\n\
2. Mirror 0Y axis.\n\
3. Mirror 0X axis.\n\
0. Back to MENU.\n\
\
Type here: ");
    if(scanf("%hd",&buffor)==1)
        return buffor;
    else
        while(getchar()!='\n');
    }
}

void menu_rotate(picture* const picture_ptr){
    bool continue_loop=true;
    short int rotate_counter=0;
    while(continue_loop){
	switch(menu_rotate_t()){
        case 1:
            printf("How many times rotate clockwise: ");
            while(1){
                if(scanf("%hd", &rotate_counter)==1)
                    break;
                else
                    while(getchar()!='\n');
            }
            switch((rotate_counter%4+4)%4){
                case 1:
                    picture_rotate_clockwise(picture_ptr);
                    break;
                case 2:
                    picture_rotate_clockwise(picture_ptr);
                    picture_rotate_clockwise(picture_ptr);
                    break;
                case 3:
                    picture_rotate_clockwise(picture_ptr);
                    picture_rotate_clockwise(picture_ptr);
                    picture_rotate_clockwise(picture_ptr);
                    break;
                case 4:
                    break;
            }
            break;
        case 2:
            picture_mirror_0Y_axis(picture_ptr);
            break;
        case 3:
            picture_mirror_0X_axis(picture_ptr);
            break;
        case 0:
            continue_loop=false;
            break;
        default:
            break;
	}
	}
}

short int menu_picture_t(void){
    short int buffor;
    while(1){
    system("clear");
    printf("\
MENU Picture:\n\
1. Add picture.\n\
2. Save selected picture.\n\
3. Select picture.\n\
4. Delete picture.\n\
0. Back to MENU.\n\
\
Type here: ");
    if(scanf("%hd",&buffor)==1)
        return buffor;
    else
        while(getchar()!='\n');
    }
}

void menu_picture(short int* picture_selected_ptr, short int* picture_counter_ptr, picture*** picture_array){
    bool continue_loop=true;
    while(continue_loop){
	switch(menu_picture_t()){
        case 1:
            *picture_array=realloc(*picture_counter_ptr==0?NULL:*picture_array,sizeof(**picture_array)*(*picture_counter_ptr+1));
            *(*picture_array+*picture_counter_ptr)=picture_add();
            *picture_counter_ptr+=1;
            *picture_selected_ptr=*picture_counter_ptr;
            break;
        case 2:
            picture_save(*(*picture_array+*picture_selected_ptr-1));
            break;
        case 3:
            picture_select(picture_selected_ptr,*picture_counter_ptr,*picture_array);
            break;
        case 4:
            if(*picture_counter_ptr==0){
                fprintf(stderr,"ERROR. Cannot delete picture, because picture base is void\n");
                getchar();
                break;
            }
            picture_delete(*picture_array, *picture_counter_ptr);
            *picture_selected_ptr=0;
            *picture_counter_ptr-=1;
            *picture_array=realloc(*picture_array,sizeof(**picture_array)*(*picture_counter_ptr+1));
            break;
        case 0:
            continue_loop=false;
            break;
        default:
            break;
	}
	}
}

short int menu_filter_t(void){
    short int buffor;
    while(1){
    system("clear");
    printf("\
MENU Filter:\n\
1. Salt & pepper filter.\n\
2. Mediana filter.\n\
3. Gauss filter.\n\
4. Negative picture.\n\
5. Progining picture.\n\
6. Genetic filter.\n\
0. Back to MENU.\n\
\
Type here: ");
    if(scanf("%hd",&buffor)==1)
        return buffor;
    else
        while(getchar()!='\n');
    }
}

void menu_filter(picture* const picture_ptr){
    bool continue_loop=true;
    while(continue_loop){
	switch(menu_filter_t()){
        case 1:
            picture_salt_pepper_filter(picture_ptr);
            break;
        case 2:
            picture_mediana_filter(picture_ptr);
            break;
        case 3:
            picture_generic_filter(picture_ptr, true);
            break;
        case 4:
            picture_negative(picture_ptr);
            break;
        case 5:
            picture_proging(picture_ptr);
            break;
        case 6:
            picture_generic_filter(picture_ptr, false);
            break;
        case 0:
            continue_loop=false;
            break;
        default:
            break;
	}
	}
}

short int menu_histogram_t(void){
    short int buffor;
    while(1){
    system("clear");
    printf("\
MENU:\n\
1. Histogram to csv.\n\
2. Append histogram on photo.\n\
3. Equalize histogram.\n\
0. Exit.\n\
\
Type here: ");
    if(scanf("%hd",&buffor)==1)
        return buffor;
    else
        while(getchar()!='\n');
    }
}

void menu_histogram(picture* const picture_ptr){
    bool continue_loop=true;
    while(continue_loop){
	switch(menu_histogram_t()){
        case 1:
            picture_histogram(picture_ptr);
            break;
        case 2:
            picture_histogram_append(picture_ptr);
            break;
        case 3:
            picture_histogram_equalization(picture_ptr);
            break;
        case 0:
            continue_loop=false;
            break;
        default:
            break;
	}
	}
}

//*************************************************************//

const picture* picture_add(void){
	picture* const picture_ptr=malloc(sizeof*picture_ptr);;
	FILE* file_ptr;
	int i,j;
	unsigned short int** picture_tab;


	if(picture_ptr==NULL){
        fprintf(stderr, "ERROR. Allocation memory problem\n");
        getchar();
        exit(1);
    }
    while(1){
        printf("Available files:\n");
        system("ls *.pgm");         //windows "dir *.pgm"
        printf("Type in file name. Remember about end .pgm and that UNIX is case sensitive\nType here: ");
        scanf("%s",picture_ptr->name);
        file_ptr=fopen(picture_ptr->name,"r");

        if(file_ptr==NULL)
            fprintf(stderr, "\nERROR. Cannot open file. Check file name.\n");
        else    break;
    }
	while(1){       //reading standard of file
		char buffor_c=fgetc(file_ptr);

		if(buffor_c=='#'){
			while(fgetc(file_ptr)=='\n');
			continue;
		}
		if (buffor_c=='P'){
			buffor_c=fgetc(file_ptr);
			picture_ptr->standard=(unsigned short int)atoi(&buffor_c);
			break;
		}
	}
	while(1){	//reading width of picture
		int buffor_i;
		if(fscanf(file_ptr,"%d",&buffor_i)==1){
			picture_ptr->width=buffor_i;
			break;
		}
		else{
			while(fgetc(file_ptr)!='\n')
			continue;
		}
	}

	while(1){	//reading height of picture
		int buffor_i;
		if(fscanf(file_ptr,"%d",&buffor_i)==1){
			picture_ptr->height=buffor_i;
			break;
		}
		else{
			while(fgetc(file_ptr)!='\n')
			continue;
		}
	}

    while(1){	//reading greyscale max
		int buffor_i;
		if(fscanf(file_ptr,"%d",&buffor_i)==1){
			picture_ptr->grey_value=buffor_i;
			break;
		}
		else{
			while(fgetc(file_ptr)!='\n')
			continue;
		}
	}

	picture_tab=malloc(picture_ptr->height*sizeof(*picture_tab));   //picture tab allocation TODO: add security check
	if(picture_tab==NULL){
        fprintf(stderr, "ERROR. Allocation memory problem\n");
        getchar();
        exit(1);
	}
	for(i=0;i<picture_ptr->height;i++){
		*(picture_tab+i)=malloc(picture_ptr->width*(sizeof(**picture_tab)));
		if(*(picture_tab+i)==NULL){
            for(;i>=0;i--){
                free(picture_tab+i);
            }
            fprintf(stderr, "ERROR. Allocation memory problem\n");
            getchar();
            exit(1);
		}
	}

	for(i=0;i<picture_ptr->height;i++){
		for(j=0;j<picture_ptr->width;){
            if(fscanf(file_ptr, "%hd", (*(picture_tab+i)+j))==1){
                j++;
			}
            else{
                while(fgetc(file_ptr)!='\n');
			}
		}
	}

    fclose(file_ptr);
	picture_ptr->tab=picture_tab;
	return picture_ptr;
}

void picture_delete(picture** picture_array, short int picture_counter){
    short int movement=0, i, picture_selected;
    picture_select(&picture_selected, picture_counter, picture_array);

    for(i=0;i<picture_counter;i++){
        int j=0;
        if(i==picture_selected){
            picture* const picture_ptr=*(picture_array+i);
            movement=-1;
            for(j=0;j<(picture_ptr->height);j++){
                free(*(picture_ptr->tab+j));
            }
            free(picture_ptr->tab);
            free(picture_ptr);
        }
        else
            *(picture_array+i)=*(picture_array+i+movement);
    }
}

void picture_save(picture* const picture_ptr){
    int i,j;
    FILE* file_ptr=NULL;
    char command_string[100]="eog ";
    strcat(command_string,picture_ptr->name);

    file_ptr=fopen(picture_ptr->name,"w");
    if(file_ptr==NULL){
        fprintf(stderr, "ERROR. File open problem\n");
        getchar();
        exit(1);
    }

    fprintf(file_ptr,"P%hd\n#Edited w/ A.B.graph\n%hd %hd\n%hd\n",picture_ptr->standard,picture_ptr->width,picture_ptr->height,picture_ptr->grey_value);

    for(i=0;i<picture_ptr->height;i++){
        for(j=0;j<picture_ptr->width;j++){
            fprintf(file_ptr,"%hd ",picture_ptr->tab[i][j]);
        }
        fprintf(file_ptr,"\n");
    }
    fclose(file_ptr);

    system(command_string);
}

void picture_show_base(short int picture_counter, picture** picture_array){
    short int i;
    system("clear");
    printf("picture base:\n");
    for(i=0;i<picture_counter;i++){
        printf("%hd:\t%s\n",i+1,((*(*(picture_array+i))).name));
    }
}

void picture_select(short int* picture_select_ptr,short int picture_counter, picture** picture_array){
    short int buffor;
    while(1){
        picture_show_base(picture_counter,picture_array);
        printf("\nSelect picture: ");
        if(scanf("%hd",&buffor)==1){
            if(buffor>picture_counter||buffor<=0)
                continue;
            else
                break;
        }
        else{
            while(getchar()!='\n');
            continue;
        }
    }
    *picture_select_ptr=buffor;
}

void picture_mirror_0Y_axis(picture* const picture_ptr){
    short int buffor,i,j;
    for(i=0;i<picture_ptr->height;i++){
        for(j=0;j<picture_ptr->width/2;j++){
            buffor=*(*(picture_ptr->tab+i)+j);
            *(*( picture_ptr->tab+i)+j)=*(*( picture_ptr->tab+i)+picture_ptr->width-j-1);
            *(*( picture_ptr->tab+i)+picture_ptr->width-j-1)=buffor;
        }
    }
}

void picture_mirror_0X_axis(picture* const picture_ptr){
    short int buffor,i,j;
    for(i=0;i<picture_ptr->height/2;i++){
        for(j=0;j<picture_ptr->width;j++){
            buffor=*(*(picture_ptr->tab+i)+j);
            *(*( picture_ptr->tab+i)+j)=*(*( picture_ptr->tab+picture_ptr->height-i-1)+j);
            *(*( picture_ptr->tab+picture_ptr->height-i-1)+j)=buffor;
        }
    }
}

void picture_negative(picture* const picture_ptr){
    short int i,j;
    for(i=0;i<picture_ptr->height;i++){
        for(j=0;j<picture_ptr->width;j++){
            *(*(picture_ptr->tab+i)+j)=abs((*(*(picture_ptr->tab+i)+j)>picture_ptr->grey_value)?(picture_ptr->grey_value):(*(*(picture_ptr->tab+i)+j))-picture_ptr->grey_value);
        }
    }
}

void picture_proging(picture* const picture_ptr){
    short int i,j, grey_value_new;

    while(1){
        printf("New max grey value: ");
        if(scanf("%hd",&grey_value_new)==1)
            if(grey_value_new>0&&grey_value_new<=picture_ptr->grey_value)
                break;
            else{
                printf("ERROR. Value have to between 0 and %hd\n", picture_ptr->grey_value);
                continue;
            }
        else
            printf("ERROR. Wrong value. Try again.\n");
    }

    for(i=0;i<picture_ptr->height;i++){
        for(j=0;j<picture_ptr->width;j++){
            picture_ptr->tab[i][j]=picture_ptr->tab[i][j]>grey_value_new?grey_value_new:picture_ptr->tab[i][j];
        }
    }
    picture_ptr->grey_value=grey_value_new;
}

void picture_histogram(picture* const picture_ptr){
    short int i;
    double* const histogram_array=picture_histogram_array(picture_ptr);
    FILE *file_dat_ptr, *file_csv_ptr;

    file_csv_ptr=fopen("picture_histogram.csv","w");
    file_dat_ptr=fopen("picture_histogram.dat","w");

    for(i=0;i<picture_ptr->grey_value+1;i++){
        fprintf(file_csv_ptr,"%hd,%lf\n",i,histogram_array[i]);
        fprintf(file_dat_ptr,"%hd\t%lf\n",i,histogram_array[i]);
    }

    free(histogram_array);
    fclose(file_csv_ptr);
    fclose(file_dat_ptr);
    system("gnuplot -p -e \"plot 'picture_histogram.dat'\"");
    system("rm picture_histogram.dat");
}

void picture_rotate_clockwise(picture* const picture_ptr){
    unsigned short int** const picture_tab=malloc(picture_ptr->width*sizeof(*picture_tab));
    unsigned short int i,j;
    int buffor;

	if(picture_tab==NULL){
        fprintf(stderr, "ERROR. Allocation memory problem\n");
        getchar();
        exit(1);
	}
	for(i=0;i<picture_ptr->width;i++){
		*(picture_tab+i)=malloc(picture_ptr->height*(sizeof(**picture_tab)));
		if(*(picture_tab+i)==NULL){
            fprintf(stderr, "ERROR. Allocation memory problem\n");
            getchar();
            exit(1);
		}
	}

	for(i=0;i<picture_ptr->width;i++){
        for(j=0;j<picture_ptr->height;j++){
            *(*(picture_tab+i)+j)=*(*( picture_ptr->tab+picture_ptr->height-j-1)+i);
            }
	}

	for(i=0;i<picture_ptr->height;i++)
		free(*(picture_ptr->tab+i));
	free(picture_ptr->tab);

	picture_ptr->tab=picture_tab;
	buffor=picture_ptr->width;
	picture_ptr->width=picture_ptr->height;
	picture_ptr->height=buffor;
}

int compare_short_int(const void *a, const void *b){
    if(*(short unsigned int*)a>*(short unsigned int*)b) return 1;
    else if(*(short unsigned int*)a<*(short unsigned int*)b) return -1;
    else return 0;
}

void picture_mediana_filter(picture* const picture_ptr){
    unsigned short int** picture_tab;
    unsigned short int i,j,k,l, *mediana_tab, mediana_filter_size;

    while(1){
        printf("Type in mediana filter size (odd and more than 1)\nType here: ");
        if(scanf("%hd", &mediana_filter_size)==1){
            if(mediana_filter_size%2&&mediana_filter_size>1)
                break;
        }
        while(getchar()!='\n');
	}
	mediana_tab=malloc(mediana_filter_size*mediana_filter_size*sizeof*mediana_tab);


    picture_tab=malloc((picture_ptr->height-2*(mediana_filter_size/2))*sizeof(*picture_tab));
    if(picture_tab==NULL){
        fprintf(stderr, "ERROR. Allocation memory problem\n");
        getchar();
        exit(1);
	}
	for(i=0;i<picture_ptr->height-2*(mediana_filter_size/2);i++){
		*(picture_tab+i)=malloc((picture_ptr->width-2*(mediana_filter_size/2))*(sizeof(**picture_tab)));
		if(*(picture_tab+i)==NULL){
            fprintf(stderr, "ERROR. Allocation memory problem\n");
            getchar();
            exit(1);
		}
	}

	for(i=0;i<picture_ptr->height-2*(mediana_filter_size/2);i++){
        for(j=0;j<picture_ptr->width-2*(mediana_filter_size/2);j++){
            for(k=0;k<mediana_filter_size;k++){
                for(l=0;l<mediana_filter_size;l++){
                    *(mediana_tab+mediana_filter_size*k+l)=*(*(picture_ptr->tab+i+k)+j+k);
                }
            }
            qsort(mediana_tab, mediana_filter_size*mediana_filter_size,sizeof *mediana_tab, compare_short_int);
            *(*(picture_tab+i)+j)=mediana_tab[mediana_filter_size*mediana_filter_size/2];
        }
	}

	for(i=0;i<picture_ptr->height;i++)
        free(*(picture_ptr->tab+i));
    free(picture_ptr->tab);

    picture_ptr->tab=picture_tab;
    picture_ptr->width-=2*(mediana_filter_size/2);
    picture_ptr->height-=2*(mediana_filter_size/2);
}

void picture_salt_pepper_filter(picture* const picture_ptr){
    short unsigned int buffor,i,j;
    srand(time(NULL));

    for(i=0;i<picture_ptr->height;i++){
        for(j=0;j<picture_ptr->width;j++){
            buffor=rand()%101;
            if(buffor==0)
                *(*(picture_ptr->tab+i)+j)=0;
            else if (buffor==100)
                *(*(picture_ptr->tab+i)+j)=picture_ptr->grey_value;
            else
                continue;
        }
    }
}

void picture_histogram_append(picture* const picture_ptr){
    unsigned int i=0,j=0;
    double* const histogram_array=picture_histogram_array(picture_ptr);
    double max_hist_value;
    unsigned short int** const array_1=calloc(sizeof(*array_1), picture_ptr->height+picture_ptr->grey_value+1);

    max_hist_value=array_biggest_double(histogram_array, picture_ptr->grey_value+1);

    if(array_1==NULL){
        fprintf(stderr,"ERROR. Memory allocation failed.");
        exit(1);
    }

    for(i=0;i<picture_ptr->height;i++){
        array_1[i]=calloc(sizeof(**array_1), picture_ptr->width);
        if(array_1[i]==NULL){
                printf("ERROR. Memory allocation problem\n.");
                getchar();
                exit(1);
        }
        for(j=0;j<picture_ptr->width;j++){
            array_1[i][j]=picture_ptr->tab[i][j];
        }
    }

    for(i=picture_ptr->height;i<picture_ptr->height+picture_ptr->grey_value+1;i++){
        array_1[i]=calloc(sizeof(**array_1), picture_ptr->width);
        if(array_1[i]==NULL){
            fprintf(stderr,"ERROR. Memory allocation failed.");
            exit(1);
        }
        for(j=0;j<=picture_ptr->width;j++){
            if((double)j/picture_ptr->width<(double)histogram_array[i-picture_ptr->height]/max_hist_value){
                array_1[i][j]=picture_ptr->grey_value;
                }
            else{
                break;
            }
        }
    }

    for(i=0;i<picture_ptr->height;i++){
        free(picture_ptr->tab[i]);
    }
    free(picture_ptr->tab);
    free(histogram_array);

    picture_ptr->height=picture_ptr->height+picture_ptr->grey_value+1;
    picture_ptr->tab=array_1;
}

double array_biggest_double(double* array, int array_size){
    unsigned int i;
    double max_value=0;

    for(i=0;i<array_size;i++){
        if(array[i]>max_value){
            max_value=array[i];
            continue;
        }
        else
            continue;
    }
    return max_value;
}

void picture_resize(picture* const picture_ptr){
    int new_width=0, new_height=0,i,j,x,y;
    double width_factor, height_factor;
    unsigned short int **picture_new_tab;

    while(1){
        printf("Old height and width are: %d\t%d\nType in new height and width: ",picture_ptr->height,picture_ptr->width);
        if(scanf("%d %d",&new_height, &new_width)==2)
            if(new_height>0&&new_width>0)
                break;
        else
            printf("ERROR. You typed in wrong values. Try again.\n");
    }

    picture_new_tab=malloc(sizeof(*picture_new_tab)*new_height);
    if(picture_new_tab==NULL){
        fprintf(stderr,"ERROR. Memory allocation problem.");
        getchar();
        exit(1);
    }

    for(i=0;i<new_height;i++){
        *(picture_new_tab+i)=malloc(sizeof(**picture_new_tab)*new_width);
        if(*(picture_new_tab+i)==NULL){
            fprintf(stderr,"ERROR. Memory allocation problem.\n");
            getchar();
            exit(1);
        }
    }

    width_factor=(double)picture_ptr->width/new_width;
    height_factor=(double)picture_ptr->height/new_height;

    for(i=0;i<new_height;i++){
        for(j=0;j<new_width;j++){
            y=floor(i*height_factor);
            x=floor(j*width_factor);
            double x_part_floor=j*width_factor-floor(j*width_factor), y_part_floor=i*height_factor-floor(i*height_factor);
            if(x<0) x=0;
            if(y<0) y=0;
            if(y>(picture_ptr->height-2)) y=picture_ptr->height-2;
            if(x>(picture_ptr->width-2)) x=picture_ptr->width-2;

            picture_new_tab[i][j]=
            picture_ptr->tab[y][x]*x_part_floor*y_part_floor+
            picture_ptr->tab[y][x+1]*(1-x_part_floor)*y_part_floor+
            picture_ptr->tab[y+1][x]*(1-y_part_floor)*x_part_floor+
            picture_ptr->tab[y+1][x+1]*(1-x_part_floor)*(1-y_part_floor);
        }
    }

    for(i=0;i<picture_ptr->height;i++){
        free(picture_ptr->tab[i]);
    }
    free(picture_ptr->tab);

    picture_ptr->height=new_height;
    picture_ptr->width=new_width;
    picture_ptr->tab=picture_new_tab;
}

void picture_histogram_equalization(picture* const picture_ptr){
    unsigned int i=0,j=0,k=0;
    double* const histogram_array=picture_histogram_array(picture_ptr);
    double *distributive_array, *look_up_table;

    look_up_table=calloc(picture_ptr->grey_value+1, sizeof(*look_up_table));
    distributive_array=calloc(picture_ptr->grey_value+1, sizeof(*distributive_array));

    if(distributive_array==NULL||look_up_table==NULL){
        fprintf(stderr,"ERROR. Memory allocation failed.");
        getchar();
        exit(1);
    }

    for(i=0; i<picture_ptr->grey_value+1;i++){
        if(i==0){
            distributive_array[i]=histogram_array[i]/(picture_ptr->height*picture_ptr->width);
        }
        else{
            distributive_array[i]=distributive_array[i-1]+histogram_array[i]/(picture_ptr->height*picture_ptr->width);
        }
    }

    for(k=0;k<picture_ptr->grey_value+1;k++){
        if(distributive_array[k]==0)
            continue;
        else
            break;
    }

    for(i=0;i<picture_ptr->grey_value+1;i++){
        look_up_table[i]=(distributive_array[i]-distributive_array[k])/(1-distributive_array[k])*(picture_ptr->grey_value);
    }

    for(i=0;i<picture_ptr->height;i++){
        for(j=0;j<picture_ptr->width;j++){
            picture_ptr->tab[i][j]=look_up_table[picture_ptr->tab[i][j]];
        }
    }

    free(distributive_array);
    free(look_up_table);
}

void picture_generic_filter(picture* const picture_ptr, bool is_gausian){
    short int filter[9]={1,2,1,2,4,2,1,2,1}, i, j;
    int buffor;
    unsigned short int** const picture_tab=malloc((picture_ptr->height-2)*sizeof(*picture_tab));

    if(picture_tab==NULL){
        fprintf(stderr, "ERROR. Allocation memory problem\n");
        getchar();
        exit(1);
	}
	for(i=0;i<picture_ptr->height-2;i++){
		*(picture_tab+i)=malloc((picture_ptr->width-2)*(sizeof(**picture_tab)));
		if(*(picture_tab+i)==NULL){
            fprintf(stderr, "ERROR. Allocation memory problem\n");
            getchar();
            exit(1);
		}
	}
    if(is_gausian==false){
    do{
        for(i=0;i<9;i++){
            printf("Type in %d:%d filter value: ", i/3+1,i%3+1);
            if(scanf("%hd", &filter[i])==1)
                continue;
            else{
                i--;
                continue;
            }
        }
    }while(filter[0]+filter[1]+filter[2]+filter[3]+filter[4]+filter[5]+filter[6]+filter[7]+filter[8]==0);
    }

	for(i=1;i<picture_ptr->height-1;i++){
        for(j=1;j<picture_ptr->width-1;j++){
            buffor=0;
            buffor+=picture_ptr->tab[i-1][j-1]*filter[0];
            buffor+=picture_ptr->tab[i][j-1]*filter[1];
            buffor+=picture_ptr->tab[i+1][j-1]*filter[2];
            buffor+=picture_ptr->tab[i-1][j]*filter[3];
            buffor+=picture_ptr->tab[i][j]*filter[4];
            buffor+=picture_ptr->tab[i+1][j]*filter[5];
            buffor+=picture_ptr->tab[i-1][j+1]*filter[6];
            buffor+=picture_ptr->tab[i][j+1]*filter[7];
            buffor+=picture_ptr->tab[i+1][j+1]*filter[8];
            *(*(picture_tab+i-1)+j-1)=buffor/(filter[0]+filter[1]+filter[2]+filter[3]+filter[4]+filter[5]+filter[6]+filter[7]+filter[8]);
        }
	}

	for(i=0;i<picture_ptr->height;i++)
        free(*(picture_ptr->tab+i));
    free(picture_ptr->tab);

    picture_ptr->tab=picture_tab;
    picture_ptr->width-=2;
    picture_ptr->height-=2;
}

double* picture_histogram_array(picture* const picture_ptr){
    int i,j;
    double* const histogram_array=calloc(picture_ptr->grey_value+1, sizeof(*histogram_array));

    for(i=0;i<picture_ptr->height;i++){
        for(j=0;j<picture_ptr->width;j++){
            histogram_array[(*(*(picture_ptr->tab+i)+j))>(picture_ptr->grey_value+1) ?picture_ptr->grey_value+1:*(*(picture_ptr->tab+i)+j)]++;
        }
    }
    return histogram_array;
}
